//
//  CategoryDummy.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import Foundation

struct CategoryDummy {
    var id: Int = -1
    var name: String = ""
    
    mutating func from(category: Category) {
        id = category.id
        name = category.name
    }
}
