//
//  AccountDummy.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import Foundation

struct AccountDummy {
    var id: Int = -1
    var name: String = ""
    var category: Category?
    
    mutating func from(account: Account) {
        id = account.id
        name = account.name
        category = account.category
    }
}
