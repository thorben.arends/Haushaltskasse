//
//  ActivityDummy.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 25.06.24.
//

import Foundation

struct ActivityDummy {
    var id: Int = -1
    var reason: String = ""
    var amount: Int = 0
    var type: ActivityType = .expense
    var recurring: RecurringType = .none
    var recurringInterval: Int = 0
    var exchangeActivity: Bool = false
    var date: Date = .now
    
    var account: Account?
    var category: Category?
    var exchangeAccount: Account?
    
    mutating func from(activity: Activity) {
        id = activity.id
        reason = activity.reason
        amount = activity.amount
        type = ActivityType.fromString(activity.type)
        recurring = RecurringType.fromString(activity.recurring)
        recurringInterval = activity.recurringInterval
        exchangeActivity = activity.exchangeActivity
        date = activity.date
        
        account = activity.account
        category = activity.category
        exchangeAccount = activity.exchangeAccount
    }
}
