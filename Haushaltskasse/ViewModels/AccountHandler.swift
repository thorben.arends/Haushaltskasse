//
//  AccountHandler.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import Foundation
import SwiftData

@Observable
class AccountHandler {
    var modelContext: ModelContext
    
    init(_ modelContext: ModelContext) {
        self.modelContext = modelContext
    }
    
    func add(from dummy: AccountDummy) {
        let id = getNextId()
        
        let account = Account(id: id, name: dummy.name, category: dummy.category)
        
        modelContext.insert(account)
    }
    
    func update(_ account: Account, with dummy: AccountDummy) {
        account.name = dummy.name
        account.category = dummy.category
    }
    
    func delete(_ account: Account) {
        modelContext.delete(account)
    }
    
    func getNextId() -> Int {
        var id = 0
        
        do {
            let predicate = #Predicate<Account> { account in true }
            let descriptor = FetchDescriptor(predicate: predicate)
            
            let results = try modelContext.fetch(descriptor)
            
            for result in results {
                if id <= result.id {
                    id = result.id + 1
                }
            }
        } catch {
            print("Fetch failed")
        }
        
        return id
    }
    
    func calculateBalance(for account: Account) -> Int {
        var balance = 0
        // TODO: Calculate Balance
        return balance
    }
    
    func calculateBalance(for accounts: [Account]) -> Int {
        var totalBalance = 0
        for account in accounts {
            totalBalance += calculateBalance(for: account)
        }
        return totalBalance
    }
    
    func calculateTotalBalance() -> Int {
        do {
            let preferencePredicate = #Predicate<IntArrayPreference> { pref in
                pref.key == "preferences.accounts.totalBalance"
            }
            let preferenceDescriptor = FetchDescriptor(predicate: preferencePredicate)
            let preference = try modelContext.fetch(preferenceDescriptor).first
            
            var accountPredicate: Predicate<Account>?
            
            if preference != nil {
                let value = preference!.value
                accountPredicate = #Predicate<Account> { account in
                    value.contains(account.id)
                }
            } else {
                accountPredicate = #Predicate<Account> { account in true }
            }
            
            let accountDescriptor = FetchDescriptor(predicate: accountPredicate)
            
            let accounts = try modelContext.fetch(accountDescriptor)
            
            return calculateBalance(for: accounts)
        } catch {
            print("Fetch failed")
            return 0
        }
    }
}
