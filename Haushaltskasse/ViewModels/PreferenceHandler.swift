//
//  PreferenceHandler.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 23.06.24.
//

import Foundation
import SwiftData

@Observable
class PreferenceHandler {
    var modelContext: ModelContext
    
    init(_ modelContext: ModelContext) {
        self.modelContext = modelContext
    }
    
    func setIntArrayPreference(key: String, value: [Int]) {
        if contains(key: key) {
            getIntArrayPreferences(for: key).first!.value = value
        } else {
            let arrayPreference = IntArrayPreference(key: key, value: value)
            modelContext.insert(arrayPreference)
        }
    }
    
    func getIntArrayPreference(for key: String) -> IntArrayPreference? {
        return getIntArrayPreferences(for: key).first
    }
    
    func getIntArrayPreferences(for key: String) -> [IntArrayPreference] {
        do {
            let predicate = #Predicate<IntArrayPreference> { pref in
                pref.key == key
            }
            let descriptor = FetchDescriptor(predicate: predicate)
            
            let results = try modelContext.fetch(descriptor)
            return results
        } catch {
            print("Fetch failed")
            return []
        }
    }
    
    func contains(key: String) -> Bool {
        let intArrayPreferences = getIntArrayPreferences(for: key)
        return !intArrayPreferences.isEmpty
    }
}
