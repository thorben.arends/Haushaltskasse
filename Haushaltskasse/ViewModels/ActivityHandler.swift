//
//  ActivityHandler.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 25.06.24.
//

import Foundation
import SwiftData

@Observable
class ActivityHandler {
    var modelContext: ModelContext
    
    init(_ modelContext: ModelContext) {
        self.modelContext = modelContext
    }
    
    func add(from dummy: ActivityDummy) {
        let id = getNextId()
        
        let activity = Activity(id: id, reason: dummy.reason, amount: dummy.type == .income ? dummy.amount : dummy.amount * -1, recurring: dummy.recurring, recurringInterval: dummy.recurringInterval, exchangeActivity: dummy.exchangeActivity, date: dummy.date, account: dummy.account, category: dummy.category, exchangeAccount: dummy.exchangeAccount)
        
        modelContext.insert(activity)
    }
    
    func update(_ activity: Activity, with dummy: ActivityDummy) {
        activity.reason = dummy.reason
        activity.amount = dummy.type == .income ? dummy.amount : dummy.amount * -1
        activity.recurring = dummy.recurring.rawValue
        activity.recurringInterval = dummy.recurringInterval
        activity.exchangeActivity = dummy.exchangeActivity
        activity.date = dummy.date
        activity.account = dummy.account
        activity.category = dummy.category
        activity.exchangeAccount = dummy.exchangeAccount
    }
    
    func delete(_ activity: Activity) {
        modelContext.delete(activity)
    }
    
    func getNextId() -> Int {
        var id = 0
        
        do {
            let predicate = #Predicate<Activity> { activity in true }
            let descriptor = FetchDescriptor(predicate: predicate)
            
            let results = try modelContext.fetch(descriptor)
            
            for result in results {
                if id <= result.id {
                    id = result.id + 1
                }
            }
        } catch {
            print("Fetch failed")
        }
        
        return id
    }
    
    func calculateBalance(for activities: [Activity]) -> Int {
        var balance = 0
        
        for activity in activities {
            balance += activity.amount
        }
        
        return balance
    }
}
