//
//  CategoryHandler.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import Foundation
import SwiftData

@Observable
class CategoryHandler {
    var modelContext: ModelContext
    
    init(_ modelContext: ModelContext) {
        self.modelContext = modelContext
    }
    
    func add(from dummy: CategoryDummy) {
        let id = getNextId()
        
        let category = Category(id: id, name: dummy.name)
        
        modelContext.insert(category)
    }
    
    func update(_ category: Category, with dummy: CategoryDummy) {
        category.name = dummy.name
    }
    
    func delete(_ category: Category) {
        modelContext.delete(category)
    }
    
    func getNextId() -> Int {
        var id = 0
        
        do {
            let predicate = #Predicate<Category> { cat in true }
            let descriptor = FetchDescriptor(predicate: predicate)
            
            let results = try modelContext.fetch(descriptor)
            
            for result in results {
                if id <= result.id {
                    id = result.id + 1
                }
            }
        } catch {
            print("Fetch failed")
        }
        
        return id
    }
}
