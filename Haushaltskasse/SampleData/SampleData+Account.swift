//
//  SampleData+Account.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import Foundation
import SwiftData

extension Account {
    static let bankAccount = Account(id: 0, name: "Sparkasse")
    static let savingsAccount = Account(id: 1, name: "Sparbuch")
}
