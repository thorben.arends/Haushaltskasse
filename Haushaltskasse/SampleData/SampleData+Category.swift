//
//  SampleData+Category.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import Foundation
import SwiftData

extension Category {
    static let realAccountsCategory = Category(id: 0, name: "Echte Konten")
    static let virtualAccountsCategory = Category(id: 1, name: "Virtuelle Konten")
    
    static func insertSampleData(modelContext: ModelContext) {
        modelContext.insert(realAccountsCategory)
        modelContext.insert(virtualAccountsCategory)
        
        modelContext.insert(Account.bankAccount)
        modelContext.insert(Account.savingsAccount)
        
        Account.bankAccount.category = realAccountsCategory
//        Account.savingsAccount.category = virtualAccountsCategory
    }
}
