//
//  Tab.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 21.06.24.
//

import Foundation

enum Tab {
    case accounts
    case categories
    case cashChecks
    case preferences
}
