//
//  RecurringType.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import Foundation

enum RecurringType: String, CaseIterable, Codable {
    case none = "none"
    case daily = "daily"
    case weekly = "weekly"
    case monthly = "monthly"
    case yearly = "yearly"
    
    static func fromString(_ string: String) -> RecurringType {
        return RecurringType(rawValue: string) ?? .none
    }
}
