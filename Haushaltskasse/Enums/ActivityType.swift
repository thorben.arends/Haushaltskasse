//
//  ActivityType.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 25.06.24.
//

import Foundation

enum ActivityType: String, CaseIterable, Codable {
    case expense = "expense"
    case income = "income"
    
    static func fromString(_ string: String) -> ActivityType {
        return ActivityType(rawValue: string) ?? .expense
    }
}
