//
//  Int+toCurrencyString.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import Foundation

extension Int {
    func toCurrencyString() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        return formatter.string(for: Decimal(self) / pow(10, 2)) ?? ""
    }
}
