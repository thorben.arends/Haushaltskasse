//
//  Category.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import Foundation
import SwiftData

@Model
class Category: Hashable {
    var id: Int = -1
    var name: String = ""
    
    @Relationship(deleteRule: .nullify, inverse: \Account.category)
    var accounts: [Account]? = [Account]()
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}
