//
//  Account.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import Foundation
import SwiftData

@Model
class Account: Hashable {
    var id: Int = -1
    var name: String = ""
    var category: Category?
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    init(id: Int, name: String, category: Category?) {
        self.id = id
        self.name = name
        self.category = category
    }
}
