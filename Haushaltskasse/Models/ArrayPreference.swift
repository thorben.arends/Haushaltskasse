//
//  ArrayPreference.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 23.06.24.
//

import Foundation
import SwiftData

@Model
class IntArrayPreference: Hashable {
    var key: String = ""
    var value: [Int] = []
    
    init(key: String, value: [Int]) {
        self.key = key
        self.value = value
    }
}
