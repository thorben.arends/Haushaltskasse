//
//  Activity.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 25.06.24.
//

import Foundation
import SwiftData

@Model
class Activity: Hashable {
    var id: Int = -1
    var reason: String = ""
    var amount: Int = 0
    var recurring: String = "none"
    var recurringInterval: Int = 0
    var exchangeActivity: Bool = false
    var date: Date = Date.now
    
    var account: Account?
    var category: Category?
    var exchangeAccount: Account?
    
    init(id: Int, reason: String, amount: Int, recurring: RecurringType, recurringInterval: Int, exchangeActivity: Bool, date: Date) {
        self.id = id
        self.reason = reason
        self.amount = amount
        self.recurring = recurring.rawValue
        self.recurringInterval = recurringInterval
        self.exchangeActivity = exchangeActivity
        self.date = date
    }
    
    init(id: Int, reason: String, amount: Int, recurring: RecurringType, recurringInterval: Int, exchangeActivity: Bool, date: Date, account: Account?, category: Category?, exchangeAccount: Account?) {
        self.id = id
        self.reason = reason
        self.amount = amount
        self.recurring = recurring.rawValue
        self.recurringInterval = recurringInterval
        self.exchangeActivity = exchangeActivity
        self.date = date
        
        self.account = account
        self.category = category
        self.exchangeAccount = exchangeAccount
    }
    
    func getRecurring() -> RecurringType {
        return RecurringType.fromString(recurring)
    }
}
