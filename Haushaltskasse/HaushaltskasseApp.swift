//
//  HaushaltskasseApp.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 12.05.24.
//

import SwiftUI
import SwiftData

@main
struct HaushaltskasseApp: App {
    var sharedModelContainer: ModelContainer = {
        let schema = Schema([
            Category.self,
            Account.self,
            IntArrayPreference.self
        ])
        let modelConfiguration = ModelConfiguration(schema: schema, isStoredInMemoryOnly: false, cloudKitDatabase: .private("iCloud.de.thorbenarends.Haushaltskasse"))
        
        do {
            return try ModelContainer(for: schema, configurations: modelConfiguration)
        } catch {
            fatalError("Could not create ModelContainer: \(error)")
        }
    }()
    
    init() {
        print(URL.applicationSupportDirectory.path(percentEncoded: false))
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        .modelContainer(sharedModelContainer)
        .environment(CategoryHandler(sharedModelContainer.mainContext))
    }
}
