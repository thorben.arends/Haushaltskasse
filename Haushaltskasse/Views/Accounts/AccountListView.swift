//
//  AccountListView.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 21.06.24.
//

import SwiftUI
import SwiftData

struct AccountListView: View {
    @Environment(AccountHandler.self) private var accountHandler
    
    @Query(sort: \Category.id) private var categories: [Category]
    
    @State private var createAccountSheetVisible = false
    
    var body: some View {
        NavigationStack {
            VStack {
                BalanceInfoTitle(balance: accountHandler.calculateTotalBalance())
                Button(action: {
                    createAccountSheetVisible = true
                }) {
                    Label("Konto hinzufügen", systemImage: "plus.circle")
                }
                
                List {
                    ForEach(categories) { category in
                        if category.accounts != nil && !category.accounts!.isEmpty {
                            Section(category.name) {
                                ForEach(category.accounts ?? []) { account in
                                    NavigationLink(value: account) {
                                        BalanceInfoField(account.name, value: accountHandler.calculateBalance(for: account))
                                    }
                                }
                            }
                        }
                    }
                }
            }
            .navigationTitle("Konten")
            .sheet(isPresented: $createAccountSheetVisible) {
                AccountEditorSheet(account: nil)
            }
            .navigationDestination(for: String.self) { account in
                ActivitiesListView()
            }
        }
    }
}

#Preview {
    let modelContainer = try! ModelContainer.sample()
    return AccountListView()
        .modelContainer(modelContainer)
        .environment(AccountHandler(modelContainer.mainContext))
}
