//
//  AccountEditorSheet.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import SwiftUI
import SwiftData

struct AccountEditorSheet: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(AccountHandler.self) private var accountHandler
    
    @State private var dummy = AccountDummy()
    
    let account: Account?
    
    var title: String {
        return account == nil ? "Neues Konto" : "Konto bearbeiten"
    }
    
    var saveButtonTitle: String {
        return account == nil ? "Erstellen" : "Speichern"
    }
    
    var body: some View {
        NavigationStack {
            Form {
                TextField("Name", text: $dummy.name)
                NavigationLink {
                    CategoryChooserView(category: $dummy.category)
                } label: {
                    ChooserField("Kategorie", selection: dummy.category?.name ?? "Bitte wählen")
                }
            }
            .navigationTitle(title)
            .toolbar {
                ToolbarItem(placement: .confirmationAction) {
                    Button(saveButtonTitle) {
                        if let account {
                            accountHandler.update(account, with: dummy)
                        } else {
                            accountHandler.add(from: dummy)
                        }
                        dismiss()
                    }
                }
                
                ToolbarItem(placement: .cancellationAction) {
                    Button("Abbrechen") {
                        dismiss()
                    }
                }
            }
        }
    }
}

#Preview {
    let modelContainer = try! ModelContainer.sample()
    return AccountEditorSheet(account: nil)
        .environment(AccountHandler(modelContainer.mainContext))
        .modelContainer(modelContainer)
}
