//
//  InfoField.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import SwiftUI

struct InfoField: View {
    var title: String
    var value: String
    
    init(_ title: String, value: String) {
        self.title = title
        self.value = value
    }
    
    var body: some View {
        HStack {
            Text(title)
            Spacer()
            Text(value)
                .bold()
        }
    }
}

struct BalanceInfoField: View {
    var title: String
    var value: Int
    
    init(_ title: String, value: Int) {
        self.title = title
        self.value = value
    }
    
    var body: some View {
        HStack {
            Text(title)
            Spacer()
            BalanceInfo(balance: value)
        }
    }
}

struct ToggleInfoField: View {
    var title: String
    var value: Bool
    
    init(_ title: String, value: Bool) {
        self.title = title
        self.value = value
    }
    
    var body: some View {
        Toggle(title, isOn: .constant(value))
            .disabled(true)
    }
}

struct DateInfoField: View {
    var title: String
    var value: Date
    
    init(_ title: String, value: Date) {
        self.title = title
        self.value = value
    }
    
    var body: some View {
        DatePicker(title, selection: .constant(value), displayedComponents: .date)
            .disabled(true)
    }
}

#Preview {
    List {
        InfoField("Grund", value: "Gehalt")
        BalanceInfoField("Kontoname", value: 100000)
        ToggleInfoField("Austausch Aktivität?", value: true)
        DateInfoField("Datum", value: Date.now)
    }
}
