//
//  BalanceInfo.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 21.06.24.
//

import SwiftUI

struct BalanceInfo: View {
    var balance: Int
    
    var color: Color {
        if balance < 0 {
            return .red
        } else if balance > 0 {
            return .green
        } else {
            return .primary
        }
    }
    
    var body: some View {
        Text(balance.toCurrencyString())
            .foregroundStyle(color)
    }
}

struct BalanceInfoTitle: View {
    var balance: Int
    
    var body: some View {
        BalanceInfo(balance: balance)
            .font(.title)
            .bold()
    }
}



#Preview {
    VStack {
        BalanceInfo(balance: 0)
        BalanceInfoTitle(balance: -1000)
    }
}
