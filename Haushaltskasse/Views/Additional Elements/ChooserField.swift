//
//  ChooserField.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import SwiftUI

struct ChooserField: View {
    var title: String
    var selection: String
    
    init(_ title: String, selection: String) {
        self.title = title
        self.selection = selection
    }
    
    var body: some View {
        HStack {
            Text(title)
            Spacer()
            Text(selection)
                .foregroundStyle(.secondary)
        }
    }
}

#Preview {
    List {
        ChooserField("Kategorie", selection: "Konten")
    }
}
