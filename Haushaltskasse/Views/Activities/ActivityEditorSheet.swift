//
//  ActivityEditorSheet.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import SwiftUI

struct ActivityEditorSheet: View {
    @Environment(\.dismiss) private var dismiss
    
    var body: some View {
        NavigationStack {
            Form {
                // TODO: Correct values (dummy)
                TextField("Grund", text: .constant(""))
                HStack {
                    Text("Betrag")
                    Spacer()
                    TextField("CURRENCY", text: .constant("0,00 €")) // TODO: CurrencyField
                } // TODO: Currency Input Field
                NavigationLink(value: "Lebensmittel") {
                    ChooserField("Kategorie", selection: "Lebensmittel")
                }
                Picker("Wiederkehrend", selection: .constant(RecurringType.monthly)) {
                    Text("Rhythmus wählen").tag(nil as RecurringType?)
                    ForEach(RecurringType.allCases, id: \.self) { type in
                        Text(type.rawValue).tag(type)
                    }
                }
                if true { // TODO: Check for recurringType != none
                    Stepper("Wiederkehr Rhythmus: 1", value: .constant(1))
                }
                Toggle("Austausch Aktivität?", isOn: .constant(true))
                NavigationLink(value: "Sparbuch") {
                    ChooserField("Austausch Konto", selection: "Sparbuch")
                }
                DatePicker("Datum", selection: .constant(Date.now), displayedComponents: .date)
                    .datePickerStyle(.automatic)
                
            }
            .navigationTitle("Neue Aktivität") // TODO: Correct title for Create or Edit
            .toolbar {
                ToolbarItem(placement: .confirmationAction) {
                    Button("Erstellen") { // TODO: Correct BUtton Text
                        // TODO: Action
                        dismiss()
                    }
                }
                
                ToolbarItem(placement: .cancellationAction) {
                    Button("Abbrechen") {
                        dismiss()
                    }
                }
            }
        }
    }
}

#Preview {
    ActivityEditorSheet()
}
