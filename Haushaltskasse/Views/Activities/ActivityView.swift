//
//  ActivityView.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import SwiftUI

struct ActivityView: View {
    @State private var editActivitySheetVisible = false
    
    var body: some View {
        Form {
            // TODO: Correct values
            InfoField("Grund", value: "Gehalt")
            BalanceInfoField("Betrag", value: 270000)
            InfoField("Kategorie", value: "Beruflich")
            InfoField("Wiederkehrend", value: "Monatlich")
            InfoField("Wiederkehr Rhythmus", value: "1")
            ToggleInfoField("Austausch Aktivität?", value: false)
            DateInfoField("Datum", value: Date.now)
        }
        .navigationTitle("Gehalt") // TODO: Correct Title
        .toolbar {
            ToolbarItem(placement: .confirmationAction) {
                Button("Bearbeiten") {
                    editActivitySheetVisible = true
                }
            }
        }
        .sheet(isPresented: $editActivitySheetVisible) {
            ActivityEditorSheet()
        }
    }
}

#Preview {
    NavigationStack {
        ActivityView()
    }
}
