//
//  ActivitiesListView.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import SwiftUI
import SwiftData

struct ActivitiesListView: View {
    @Query private var activities: [Activity]
    
    @State private var newActivitySheetVisible = false
    
    let account: Account
    
    init(account: Account) {
        self.account = account
        
        let predicate = #Predicate<Activity> { activity in
            
        }
    }
    
    var body: some View {
        VStack {
            BalanceInfoTitle(balance: 100000) // TODO: Correct total balance
            Button(action: {
                newActivitySheetVisible = true
            }) {
                Label("Aktivität hinzufügen", systemImage: "plus.circle")
            }
            
            List {
                NavigationLink(value: "Gehalt") { // TODO: Correct Activity value
                    BalanceInfoField("Gehalt", value: 270000) // TODO: Correct values
                }
                
                NavigationLink(value: "Miete") { // TODO: Correct Activity value
                    BalanceInfoField("Miete", value: -120000) // TODO: Correct values
                }
                
                NavigationLink(value: "Einkaufsgeld") { // TODO: Correct Activity value
                    BalanceInfoField("Einkaufsgeld", value: -50000) // TODO: Correct values
                }
            }
        }
        .navigationTitle("Sparkasse") // TODO: Correct title
        .sheet(isPresented: $newActivitySheetVisible) {
            ActivityEditorSheet()
        }
        .navigationDestination(for: String.self) { activity in
            ActivityView()
        }
    }
}

#Preview {
    let modelContainer = try! ModelContainer.sample()
    return NavigationStack {
        ActivitiesListView(account: Account.bankAccount)
            .modelContainer(modelContainer)
            .environment(AccountHandler(modelContainer.mainContext))
    }
}
