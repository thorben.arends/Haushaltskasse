//
//  AccountsPreferences.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import SwiftUI
import SwiftData

struct AccountsPreferences: View {
    @Environment(PreferenceHandler.self) private var preferenceHandler
    
    @Query(sort: \Account.id) private var accounts: [Account]
    
    var body: some View {
        List {
            Section("Konten für Gesamtkontostand") {
                let preference = preferenceHandler.getIntArrayPreference(for: "preferences.accounts.totalBalance")
                ForEach(accounts) { account in
                    let contains = preference != nil && preference!.value.contains(account.id)
                    var ids = preference != nil ? preference!.value : [Int]()
                    Button {
                        if ids.contains(account.id) {
                            ids.removeAll(where: { $0 == account.id })
                        } else {
                            ids.append(account.id)
                        }
                        
                        preferenceHandler.setIntArrayPreference(key: "preferences.accounts.totalBalance", value: ids)
                    } label: {
                        Label(account.name, systemImage: contains ? "checkmark.circle.fill" : "circle")
                    }
                    .foregroundStyle(.primary)
                }
            }
        }
        .navigationTitle("Konten")
    }
}

#Preview {
    let modelContainer = try! ModelContainer.sample()
    return NavigationStack {
            AccountsPreferences()
        }
        .modelContainer(modelContainer)
        .environment(PreferenceHandler(modelContainer.mainContext))
}
