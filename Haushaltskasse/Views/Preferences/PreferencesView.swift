//
//  PreferencesView.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import SwiftUI

struct PreferencesView: View {
    var body: some View {
        NavigationStack {
            List {
                NavigationLink {
                    AccountsPreferences()
                } label: {
                    Label("Konten", systemImage: "creditcard.circle.fill")
                }
            }
            .navigationTitle("Einstellungen")
        }
    }
}

#Preview {
    PreferencesView()
}
