//
//  ContentView.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 12.05.24.
//

import SwiftUI
import SwiftData

struct ContentView: View {
    @State private var tab: Tab = .accounts
    
    var body: some View {
        TabView(selection: $tab) {
            AccountListView()
                .tag(Tab.accounts)
                .tabItem {
                    Label("Konten", systemImage: "creditcard.circle.fill")
                }
            
            CategoriesListView()
                .tag(Tab.categories)
                .tabItem {
                    Label("Kategorien", systemImage: "list.bullet.circle.fill")
                }
            
            PreferencesView()
                .tag(Tab.preferences)
                .tabItem {
                    Label("Einstellungen", systemImage: "gear.circle.fill")
                }
        }
    }
}

#Preview {
    let modelContainer = try! ModelContainer.sample()
    return ContentView()
        .environment(CategoryHandler(modelContainer.mainContext))
        .environment(AccountHandler(modelContainer.mainContext))
        .modelContainer(modelContainer)
}
