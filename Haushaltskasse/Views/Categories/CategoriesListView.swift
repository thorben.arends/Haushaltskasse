//
//  CategoriesListView.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import SwiftUI
import SwiftData

struct CategoriesListView: View {
    @Environment(CategoryHandler.self) private var categoryHandler
    
    @Query(sort: \Category.id) private var categories: [Category]
    
    @State private var createCategoryVisible = false
    
    var body: some View {
        NavigationStack {
            VStack {
                Button(action: {
                    createCategoryVisible = true
                }) {
                    Label("Kategorie hinzufügen", systemImage: "plus.circle")
                }
                
                List {
                    ForEach(categories) { category in
                        Text(category.name)
                    }
                    .onDelete { indexSet in
                        for index in indexSet {
                            categoryHandler.delete(categories[index])
                        }
                    }
                }
            }
            .navigationTitle("Kategorien")
            .sheet(isPresented: $createCategoryVisible) {
                CategoryEditorSheet(category: nil)
            }
        }
    }
}

#Preview("Preview - CategoriesListView") {
    let modelContainer = try! ModelContainer.sample()
    return CategoriesListView()
        .modelContainer(modelContainer)
        .environment(CategoryHandler(modelContainer.mainContext))
}
