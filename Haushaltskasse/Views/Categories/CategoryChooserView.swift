//
//  CategoryChooserView.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import SwiftUI
import SwiftData

struct CategoryChooserView: View {
    @Environment(\.dismiss) private var dismiss
    
    @Query(sort: \Category.id) private var categories: [Category]
    
    @Binding var category: Category?
    
    var body: some View {
        List {
            ForEach(categories) { category in
                Button {
                    self.category = category
                    dismiss()
                } label: {
                    Label(category.name, systemImage: self.category != nil && category.id == self.category!.id ? "checkmark.circle.fill" : "circle")
                }
                .foregroundStyle(.primary)
            }
        }
        .navigationTitle("Kategorie wählen")
    }
}

#Preview {
    let modelContainer = try! ModelContainer.sample()
    return NavigationStack {
        CategoryChooserView(category: .constant(Category.virtualAccountsCategory))
            .modelContainer(modelContainer)
    }
}
