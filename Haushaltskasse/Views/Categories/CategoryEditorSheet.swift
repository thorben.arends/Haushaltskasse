//
//  CategoryEditorSheet.swift
//  Haushaltskasse
//
//  Created by Thorben Arends on 22.06.24.
//

import SwiftUI
import SwiftData

struct CategoryEditorSheet: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(CategoryHandler.self) private var categoryHandler
    
    let category: Category?
    
    @State private var categoryDummy = CategoryDummy()
    
    var title: String {
        return category != nil ? "Kategorie bearbeiten" : "Neue Kategorie"
    }
    
    var saveButtonTitle: String {
        return category != nil ? "Speichern" : "Erstellen"
    }
    
    var body: some View {
        NavigationStack {
            Form {
                TextField("Name", text: $categoryDummy.name)
            }
            .navigationTitle(title)
            .onAppear {
                withAnimation {
                    if let category {
                        categoryDummy.from(category: category)
                    }
                }
            }
            .toolbar {
                ToolbarItem(placement: .confirmationAction) {
                    Button(saveButtonTitle) {
                        if let category {
                            categoryHandler.update(category, with: categoryDummy)
                        } else {
                            categoryHandler.add(from: categoryDummy)
                        }
                        dismiss()
                    }
                }
                
                ToolbarItem(placement: .cancellationAction) {
                    Button("Abbrechen") {
                        dismiss()
                    }
                }
            }
        }
    }
}

#Preview {
    let modelContainer = try! ModelContainer.sample()
    return CategoryEditorSheet(category: nil)
        .environment(CategoryHandler(modelContainer.mainContext))
        .modelContainer(modelContainer)
}
